terraform {
  backend "s3" {
    bucket     = "terraform-backend-bsem"
    key        = "terraform.tfstate"
    region     = "eu-central-1"
  }
}
