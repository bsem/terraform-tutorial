module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name          = "module-instance-2"
  ami           = "ami-0b7fd829e7758b06d"
  instance_type = "t2.micro"
  subnet_id     = "subnet-004550ffb6ee49107"
}